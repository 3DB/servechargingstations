"""
A server which serves information to his charging stations.

:author: Thogs
:date: 4.9.18
"""

import os.path

from flask import Flask
from flask import abort, jsonify
from flask_classy import FlaskView, route

class ServesChargingStations(FlaskView):

    station_dir = "Stations"

    @route("/charging_station/<station_id>", methods=["POST", "GET"])
    def charging_station(self, station_id):
        file_name = os.path.join(self.station_dir, station_id)
        if os.path.isfile(file_name):
            with open(file_name, "r") as f:
                return jsonify(f.read())

        else:
            abort(400)


app = Flask(__name__)

ServesChargingStations.register(app, route_base='/')
app.run(host="0.0.0.0", port=5010)
